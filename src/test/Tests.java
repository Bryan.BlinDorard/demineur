// import java.util.List;
// import org.junit.*;
// import static org.junit.Assert.*;

// public class Tests {
//     //Note : tests non fonctionnel depuis le passage en pattern MVC, se référer au jeu du démineur en mode terminal de commande.
//     /**
//      * une grille 3x3 avec 4 bombes au Nord,Sud,Ouest,Est
//      */
//     private Grille grille1;

//     /**
//      * la case qui est le milieu de la grille 1, sans bombes
//      */
//     private Case caseMilieu1;

//     /**
//      * une grille 3x3 avec 2 bombes dont une bombe placée milieu-gauche et une autre aléatoirement autour de cette case
//      */
//     private Grille grille2;

//     /**
//      * la case qui est le milieu-gauche de la grille 2, avec bombe
//      */
//     private Case caseUnDeux;


//     /**
//      * On initialise les deux grilles 3x3 (une avec 4 bombes et l'autre avec 2 bombes) 
//      * avec les deux cases sans bombes correspondant le milieu de la grille
//      */
//     @Before
//     public void initialisation(){
//         //Partie 1 : On crée une grille de 3x3 avec 4 bombes
//         boolean fin = false;
//         while(!(fin)){
//             this.grille1 = new Grille(3, 3, 4);
//             this.caseMilieu1 = this.grille1.getCase(1, 1);

//             if(!(this.caseMilieu1.estBombe())){
//                 fin = true;
//             }
//         }

//         //Partie 2 : On crée une grille de 3x3 avec 2 bombes (dont une bombe au milieu-droite)
//         fin = false;
//         while(!(fin)){
//             this.grille2 = new Grille(3, 3, 2);
//             this.caseUnDeux = this.grille2.getCase(1, 2);

//             if(this.caseUnDeux.estBombe() && (this.grille2.getCase(0, 1).estBombe() || this.grille2.getCase(0, 2).estBombe() || 
//                                                      this.grille2.getCase(1, 1).estBombe() || 
//                                                      this.grille2.getCase(2, 1).estBombe() || this.grille2.getCase(2, 2).estBombe())){
//                 fin = true;
//             }
//         }
//     }

//     //Pour la grille
//     /**
//      * Test de la fonction getNombreDeCasesRevelees() de la Grille
//      */
//     @Test
//     public void testGetNombreDeCasesRevelees(){
//         this.grille1.getCase(0, 1).reveler();
//         this.grille1.getCase(2, 0).reveler();
//         this.grille1.getCase(2, 2).reveler();

//         this.grille2.getCase(0, 0).reveler();
//         this.grille2.getCase(1, 0).reveler();
//         this.grille2.getCase(1, 2).reveler();

//         assertEquals(this.grille1.getNombreDeCasesRevelees(), 3);
//         assertEquals(this.grille2.getNombreDeCasesRevelees(), 3);
//     }

//     /**
//      * Test de la fonction getNombreDeCasesMarquees() de la classe Grille
//      */
//     @Test
//     public void testGetNombreDeCasesMarquees(){
//         this.grille1.getCase(1, 0).marquer();
//         this.grille1.getCase(1, 2).marquer();
//         this.grille1.getCase(2, 1).marquer();

//         this.grille2.getCase(0, 2).marquer();
//         this.grille2.getCase(1, 1).marquer();
//         this.grille2.getCase(2, 0).marquer();

//         assertEquals(this.grille1.getNombreDeCasesMarquees(), 3);
//         assertEquals(this.grille2.getNombreDeCasesMarquees(), 3);
//     }

//     /**
//      * Test de la fonction estPerdue() de la classe Grille
//      */   
//     @Test
//     public void testEstPerdue(){
//         assertFalse(this.grille1.estPerdue());
//         assertFalse(this.grille2.estPerdue());
//         this.caseMilieu1.reveler();
//         this.caseUnDeux.reveler();
//         assertFalse(this.grille1.estPerdue());
//         assertTrue(this.grille2.estPerdue());
//     }

//     /**
//      * Test de la fonction estGagnee() de la classe Grille
//      */   
//     @Test
//     public void testEstGagnee(){
//         assertFalse(this.grille1.estGagnee());
//         assertFalse(this.grille2.estGagnee());

//         Case laCase1 = null;
//         Case laCase2 = null;
//         for(int i = 0 ; i < 3 ; ++i){
//             for(int j = 0 ; j < 3 ; ++j){
//                 laCase1 = this.grille1.getCase(i, j);
//                 laCase2 = this.grille2.getCase(i, j);
//                 if(!(laCase1.estBombe())){
//                     laCase1.reveler();
//                 }
//                 if(laCase2.estBombe()){
//                     laCase2.marquer();
//                 }
//             }
//         }
//         assertTrue(this.grille1.estGagnee());
//         assertTrue(this.grille2.estGagnee());
//     }

   

//     //Pour les cases
//     /**
//      * Test de la fonction ajouteBombe() de la classe Case
//      * sachant que la case du milieu de la grille 1 ne contient pas de bombe
//      * sachant que la case du milieu de la grille 2 contient une bombe
//      */
//     @Test
//     public void testAjouteBombe(){
//         assertFalse(this.caseMilieu1.estBombe());
//         assertTrue(this.caseMilieu1.ajouteBombe());
//         assertTrue(this.caseMilieu1.estBombe());
        
//         assertTrue(this.caseUnDeux.estBombe());
//         assertFalse(this.caseUnDeux.ajouteBombe());
//         assertTrue(this.caseUnDeux.estBombe());
//     }

//     /**
//      * Test de la fonction getCasesVoisines() de la classe Case
//      * sachant que la case du milieu-droite de la grille 2 n'a que 5 voisins puisqu'il est au bord du plateau
//      */
//     @Test
//     public void testGetCasesVoisines(){
//         List<Case> casesVoisines1 = this.caseMilieu1.getCasesVoisines();
//         List<Case> casesVoisines2 = this.caseUnDeux.getCasesVoisines();
//         int nb1 = 0;
//         int nb2 = 0;
//         for(int i = 0 ; i < this.grille1.getNombreDeLignes() ; ++i){
//             for(int j = 0 ; j < this.grille1.getNombreDeColonnes() ; ++j){
//                 if(!(i == 1 && j == 1)){
//                     assertTrue(casesVoisines1.contains(this.grille1.getCase(i, j)));
//                     nb1 += 1;
//                 }       
//                 if(!(i == 1 && j == 2) && !(i == 0 && j == 0) && !(i == 1 && j == 0) && !(i == 2 && j == 0)){
//                     assertTrue(casesVoisines2.contains(this.grille2.getCase(i, j)));
//                     nb2 += 1;
//                 }
//             }   
//         }
//         assertEquals(nb1, 8);
//         assertEquals(nb2, 5);
//     }

//     /**
//      * Test de la fonction getNbBombesVoisines() de la classe Case
//      */
//     @Test
//     public void testGetNbBombesVoisines(){
//         assertEquals(this.caseMilieu1.getNbBombesVoisines(), 4);
//         assertEquals(this.caseUnDeux.getNbBombesVoisines(), 1); //1 car on ne doit pas compter la case qui cherche les bombes de ses voisins
//     }

//     /**
//      * Test de la fonction getAffichage() de la classe Case
//      */
//     @Test
//     public void testGetAffichage(){
//         this.caseMilieu1.reveler();
//         assertEquals(this.caseMilieu1.getAffichage(), "4");
//         assertEquals(this.caseUnDeux.getAffichage(), " ");
//         this.grille2.getCase(0, 0).marquer();
//         assertEquals(this.grille2.getCase(0, 0).getAffichage(), "?");
//         this.caseUnDeux.reveler();
//         assertEquals(this.caseUnDeux.getAffichage(), "@");
//     }
// }

